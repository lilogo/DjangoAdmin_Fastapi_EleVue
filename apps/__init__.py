# +----------------------------------------------------------------------
# | DjangoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
# +----------------------------------------------------------------------
# | 版权所有 2021~2023 北京DjangoAdmin研发中心
# +----------------------------------------------------------------------
# | Licensed LGPL-3.0 DjangoAdmin并不是自由软件，未经许可禁止去掉相关版权
# +----------------------------------------------------------------------
# | 官方网站: https://www.djangoadmin.cn
# +----------------------------------------------------------------------
# | 作者: @一米阳光 团队荣誉出品
# +----------------------------------------------------------------------
# | 版权和免责声明:
# | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
# | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
# | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
# | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
# | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
# | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
# | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
# | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
# | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
# +----------------------------------------------------------------------

import time
import traceback

from fastapi import FastAPI
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.logger import logger
from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse

from extends.extends_exception import AuthorizationException
from extends import register_extends
from routers import register_router
from apps.middleware import register_middleware, register_cros, register_redis


# 创建应用
def create_app() -> FastAPI:
    # # 实例化应用并鉴权
    app = FastAPI(title="FastAPI",
                  description="FastAPI",
                  version="v1",
                  )

    # 注册跨域中间件
    register_cros(app)
    # 进行挂载
    register_redis(app)
    # 注册路由
    register_router(app)
    # 初始化扩展
    register_extends(app)
    # 登录检测
    register_middleware(app)
    # 注册全局异常处理
    register_exception(app)

    # 自定义中间件
    @app.middleware("http")
    async def add_process_time_header(request: Request, call_next):
        # 开始时间
        start_time = time.time()
        response = await call_next(request)
        # 过程时间
        process_time = time.time() - start_time
        # X- 作为前缀代表专有自定义请求头
        response.headers["X-Process-Time"] = str(round(process_time, 5))
        logger.info(f"访问记录:{request.method} url:{request.url}  耗时:{str(round(process_time, 5))}")
        # 放行
        return response

    # 返回应用
    return app


# 注册全局异常处理
def register_exception(app: FastAPI):
    """
    全局异常捕获
    :param app:
    :return:
    """

    # 登录认证异常处理执行句柄
    @app.exception_handler(AuthorizationException)
    async def unicorn_exception_handler(request: Request, exc: AuthorizationException):
        return JSONResponse(
            status_code=200,
            content={
                "code": exc.code,
                "msg": exc.msg,
            },
        )

    # # 捕获参数 验证错误
    # @app.exception_handler(RequestValidationError)
    # async def validation_exception_handler(request: Request, exc: RequestValidationError):
    #     """
    #     捕获请求参数 验证错误
    #     :param request:
    #     :param exc:
    #     :return:
    #     """
    #     logger.error(f"参数错误\nURL:{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")
    #     return JSONResponse(
    #         status_code=status.HTTP_400_BAD_REQUEST,
    #         content=jsonable_encoder({"code": 10002, "data": {"tip": exc.errors()}, "body": exc.body,
    #                                   "message": "参数不全或参数错误"}),
    #     )
    #
    # # 捕获全部异常
    # @app.exception_handler(Exception)
    # async def all_exception_handler(request: Request, exc: Exception):
    #     logger.error(f"全局异常\nURL:{request.url}\nHeaders:{request.headers}\n{traceback.format_exc()}")
    #     return JSONResponse(
    #         status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    #         content={"code": 500, "data": {"tip": "服务器错误"}, "message": "fail"},
    #     )
    #
    # # 捕获断言错误，用于返回错误状态
    # @app.exception_handler(AssertionError)
    # async def asser_exception_handler(request: Request, exc: AssertionError):
    #     logger.error(f"断言错误，URL：{request.url}, 此处条件不符合")
    #     logger.info(f"------------------------{exc.args}")
    #     state = exc.args[0] if exc.args else 0
    #     return JSONResponse(
    #         status_code=state,
    #         content={"code": 500, "data": {"tip": "服务器错误"}, "message": "fail"},
    #     )
    #     # return JSONResponse(res(state=state))
